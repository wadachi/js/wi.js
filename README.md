# wi.js

A class oriented JavaScript framework for the browser.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [NPM](https://www.npmjs.com/)
* [UglifyJS](http://lisperator.net/uglifyjs/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`

## Build

* uglifyjs wi.js -o wi.min.js -p 5 -c -m --comments
