/* @license
  Wi.js
  (C) Copyright 2015 Wadachi, Inc. 株式会社わだち
  Licensed under the Apache License, Version 2.0

*/

// フレームワーク名　前空間
window.Wi = (function($) {
  "use strict";

  // クラスの文字列式
  var classToString = function() {
    return this.className || "Class";
  };

  // クラスの文字列式
  var classInstanceToString = function() {
    return this.constructor.className || "Class";
  };

  // 空の関数、常にthisを返す
  var empty = function() {
    return this;
  };

  // クラスを拡張する・クラスビルダ
  var extend = function(instanceMembers, staticMembers) {
    var className, ctor, key, pctor, ptype;

    className = instanceMembers["className"];
    delete instanceMembers["className"];

    pctor = this;
    ptype = Object.create(this.prototype);
    ctor = makeCtor(ptype, className);

    // 静的メンバのコピーとバインド
    for (key in staticMembers)
      ctor[key] = staticMembers[key];

    for (key in pctor) {
      if (typeof ctor[key] === "undefined")
        Object.defineProperty(ctor, key, {
          configurable: true,
          enumerable: true,
          get: (function(key) {
            return function() {
              return pctor[key];
            };
          })(key)
        });
    }

    // インスタンスメンバのコピー
    for (key in instanceMembers) {
      if (typeof ptype[key] === "function")
        ptype[key] = wrap(ptype[key], instanceMembers[key]);
      else
        ptype[key] = instanceMembers[key];
    }

    return ctor;
  };

  // コンストラクタを作る
  var makeCtor = function(ptype, className) {
    var Class;

    Class = function() {
      var descriptor, key, obj;

      if (!(this instanceof Class))
        throw new Error("Constructors must be called in conjunction with new");

      // バインディング
      for (key in this) {
        obj = this[key];
        if (typeof(obj) === "function") {
          obj = obj.bind(this);
          descriptor = { configurable: true, value: obj, writable: true };
          Object.defineProperty(this, key, descriptor);
        }
      }

      if (typeof(this.init) === "function")
        return this.init.apply(this, arguments) || this;
      else
        return this;
    };

    Class.prototype = ptype;
    Object.defineProperty(ptype, "constructor", { value: Class });
    Object.defineProperty(Class, "className", { value: className || "Class" });
    Object.defineProperty(Class, "extend", { value: extend });
    Object.defineProperty(Class, "toString", { value: classToString });

    return Class;
  };

  // 機能をラップして継承されたメンバ関数へのアクセスを提供する
  var wrap = function(superFn, overrideFn) {
    return function() {
      var key, prior;

      key = "_super";
      prior = Object.getOwnPropertyDescriptor(this, key);

      try {
        Object.defineProperty(this, key, { configurable: true, value: superFn.bind(this) });
        overrideFn.apply(this, arguments);
      } finally {
        if (prior)
          Object.defineProperty(this, key, prior);
        else
          delete this[key];
      }
    }
  };

  // 基本的なクラス
  var Class = makeCtor({
    toString: classInstanceToString
  }, "Class", empty);

  // 名前空間
  var Namespace = Class.extend({
    className: "Namespace",

    // 初期化
    init: function() {
      var i, key;

      // 名前空間にメンバーを追加する
      for (i in arguments)
        for (key in arguments[i])
          this[key] = arguments[i][key];
    }
  });

  // NULLログ　クラス
  var NullLog = Class.extend({
    className: "NullLog",

    // コンソールにアサーションメッセージを表示する
    assert: empty,

    // コンソールにデバッグメッセージを表示する
    debug: empty,

    // コンソールに情報メッセージを表示する
    error: empty,

    // コンソールに情報メッセージを表示する
    info: empty,

    // コンソールに警告メッセージを表示する
    warn: empty
  });

  // ビジュアルID
  var nextVisualId = 0;

  // ビジュアル　クラス
  var Visual = Class.extend({
    className: "Visual",
    classes: [],
    element: "div",
    id: null,
    options: null,

    // 初期化
    init: function(viewParent, options) {
      var template;

      this.options = $.extend({}, options);
      this.id = this.id || "visual-" + nextVisualId++;

      template = this.interpolate("<{{element}} id='{{id}}' class='{{classes}}'></{{element}}>", {
        element: this.element,
        id: this.id,
        classes: this.classes.concat(this.options.classes || []).join(" ")
      });

      this.view = $(template);
      this.$ = this.view.find.bind(this.view);
      $(viewParent).append(this.view);
    },

    // 最終化
    destroy: function() {
      this.$('*').off();
      this.view.off();
      this.view.remove();
      this.view = $();
      this.$ = this.view.find.bind(this.view);
    },

    // インライン ドキュメント コンパイラ
    heredoc: function(fn) {
      var text;

      text = (Function.toString.apply(fn).match(/\/\**\s*?\@preserve\s*([\s\S]*?)\s*\*\//m) || [])[1];
      text = text || (Function.toString.apply(fn).match(/\/\*\s*([\s\S]*?)\s*\*\//m) || [])[1];

      return text;
    },

    // 指定されたコンテキストからの値を持つ文字列を補間する
    interpolate: function(str, context) {
      for (var key in context)  // splitとjoinですべて置換
        str = str.split("{{" + key + "}}").join(context[key]);

      return str;
    },
  });

  // フレームワーク　名前空間
  var Wi = new Namespace({
    Class: Class,
    Namespace: Namespace,
    NullLog: NullLog,
    Visual: Visual
  });

  return Wi;
})(window.jQuery);
